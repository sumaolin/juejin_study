import React from 'react'
import { Button } from 'zarm'

import { get } from 'utils'

import s from './style.module.less'

export default function Index() {
  const getData = () => {
    get('/userInfo').then((res) => {
      console.log('userInfo: ', res)
    })
  }
  return (
    <div className={s.index}>
      <span>Index</span>
      <Button
        theme='primary'
        onClick={(e) => {
          e.preventDefault()
          getData()
        }}>
        按钮
      </Button>
    </div>
  )
}
