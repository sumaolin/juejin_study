# Node + React 实战：从 0 到 1 实现记账本

#### 介绍

https://juejin.cn/book/6966551262766563328

#### 软件架构

1. server 目录下是 基于教程中的eggjs 框架写的 后端 API
2. client 基于 react hooks 写法的 前端解密

#### 安装教程

1.  git clone 项目
2.  cd server 目录下`npm i` 和 `npm run dev` 启动后端 API，地址默认是：http://localhost:7001/
3.  cd client 目录下`npm i` 和 `npm run dev` 启动前端项目  地址默认是：http://localhost:3000/

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### issue



1. [Uncaught SyntaxError: The requested module ‘/node_modules/.vite/react-router……Switch | DebugAH](https://debugah.com/uncaught-syntaxerror-the-requested-module-node_modules-vitereact-router-switch-22991/) 

   React-router-dom 版本问题

2. [vite-plugin-style-import/README.zh_CN.md at main · vbenjs/vite-plugin-style-import (github.com)](https://github.com/vbenjs/vite-plugin-style-import/blob/main/README.zh_CN.md)

   按文档中配置会报错，降级到1.4.0就OK了
