const Service = require('egg').Service

class HomeService extends Service {
  async user() {
    const { ctx, app } = this
    const sql = 'select id, name from list'
    console.log(app.mysql)
    try {
      const result = await app.mysql.query(sql)
      return result
    } catch (error) {
      console.log('service error', error)
      return null
    }
    // return { name: 'ceshi', slogan: '记账本' }
  }

  async addUser(name) {
    const { app } = this
    try {
      const result = app.mysql.insert('list', { name })
      return result
    } catch (error) {
      return null
    }
  }

  async editUser(id, name) {
    const { app } = this
    try {
      const result = app.mysql.update(
        'list',
        {
          name,
        },
        {
          where: { id },
        }
      )
      return result
    } catch (error) {
      return null
    }
  }

  async deleteUser(id) {
    const { app } = this
    try {
      const result = await app.mysql.delete('list', { id })
      return result
    } catch (error) {
      return null
    }
  }
}

module.exports = HomeService
