const Service = require('egg').Service

class TypeService extends Service {
  async queryList() {
    const { app } = this

    try {
      const QUERY_STR = 'select id, name , type from type where user_id = 0'
      const result = await app.mysql.query(QUERY_STR)
      return result
    } catch (error) {
      console.log('service type error', error)
      return null
    }
  }
}

module.exports = TypeService
