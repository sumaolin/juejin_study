const Service = require('egg').Service

class BillService extends Service {
  async addBill(billItem) {
    const { app } = this
    try {
      const result = await app.mysql.insert('bill', {
        ...billItem,
      })
      return result
    } catch (error) {
      return null
    }
  }

  async getDataByUserId(userId) {
    const { app } = this
    const QUERY_STR = `select id, date, pay_type, amount, type_id, type_name, remark from bill where user_id = ${userId}`

    try {
      const result = app.mysql.query(QUERY_STR)
      return result
    } catch (error) {
      console.log(error)
      return null
    }
  }

  async detail(id, user_id) {
    const { ctx, app } = this
    try {
      const result = await app.mysql.get('bill', { id, user_id })
      return result
    } catch (error) {
      console.log(error)
      return null
    }
  }

  async update(params) {
    const { ctx, app } = this
    try {
      let result = await app.mysql.update(
        'bill',
        {
          ...params,
        },
        {
          id: params.id,
          user_id: params.user_id,
        }
      )
      return result
    } catch (error) {
      console.log(error)
      return null
    }
  }

  async delete(id, user_id) {
    const { ctx, app } = this
    try {
      let result = await app.mysql.delete('bill', {
        id: id,
        user_id: user_id,
      })
      return result
    } catch (error) {
      console.log(error)
      return null
    }
  }
}

module.exports = BillService
