const Service = require('egg').Service

class UserService extends Service {
  /**
   * 通过用户名称获取用户信息
   * @param {string} username 用户名称
   */
  async findUserByName(username) {
    const { app } = this
    try {
      const result = await app.mysql.get('user', { username })
      return result
    } catch (error) {
      console.log(error)
      return null
    }
  }

  async register(userInfo) {
    const { app } = this
    try {
      const result = await app.mysql.insert('user', userInfo)
      return result
    } catch (error) {
      console.log(error)
      return null
    }
  }

  async editUserInfo(userInfo) {
    const { ctx, app } = this
    try {
      const result = app.mysql.update(
        'user',
        { ...userInfo },
        {
          id: userInfo.id,
        }
      )
      return result
    } catch (error) {
      console.log(error)
      return null
    }
  }
}

module.exports = UserService
