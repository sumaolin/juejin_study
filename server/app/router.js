'use strict'

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = (app) => {
  const { router, controller, middleware } = app
  const _jwt = middleware.jwtErr(app.config.jwt.secret)
  router.get('/', controller.home.index)
  router.get('/user/:id', controller.home.user)
  router.post('/add', controller.home.create)
  router.post('/user/add', controller.home.addUser)
  router.put('/user/edit', controller.home.editUser)
  router.delete('/user/:id', controller.home.deleteUser)
  router.post('/api/user/register', controller.user.register)
  router.post('/api/user/login', controller.user.login)
  router.get('/api/user/decode', _jwt, controller.user.decodeToken)
  router.get('/api/user/info', _jwt, controller.user.getUserInfo)
  router.post('/api/user/edit', _jwt, controller.user.editUserInfo)
  router.post('/api/user/modify_pass', _jwt, controller.user.modifyPassword)
  router.post('/api/upload', controller.upload.upload)
  // bill api
  router.post('/api/bill/add', _jwt, controller.bill.add)
  router.get('/api/bill/list', _jwt, controller.bill.getBillList)
  router.get('/api/bill/detail', _jwt, controller.bill.detail)
  router.put('/api/bill/edit', _jwt, controller.bill.editBill)
  router.del('/api/bill/del', _jwt, controller.bill.deleteBillItem)
  router.get('/api/bill/data', _jwt, controller.bill.summaryData)
  // type api 账单类型 API
  router.get('/api/type/list', controller.type.list)
}
