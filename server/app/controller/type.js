const Controller = require('egg').Controller

class TypeController extends Controller {
  async list() {
    const { ctx } = this

    try {
      const result = await ctx.service.type.queryList()
      console.log(result)
      ctx.body = {
        code: 200,
        msg: '查询成功',
        data: {
          list: result,
        },
      }
    } catch (error) {
      console.log('type list', error)
      ctx.body = {
        code: 500,
        msg: '系统错误',
        data: null,
      }
    }
  }
}

module.exports = TypeController
